class Student
  attr_reader :first_name, :last_name
  attr_accessor :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def courses
    @courses
  end

  def enroll(course)
    @courses.each do |course1|
      if course1.conflicts_with?(course)
        raise error
      end
    end

    unless @courses.include?(course)
      @courses << course
      course.students << self
    end
  end

  def course_load
    course_load = Hash.new(0)

    @courses.each do |course|
      course_load[course.department] += course.credits
    end

    course_load
  end
end
